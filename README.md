# lesson9

Задание: 
Написать плейбук для настройки nginx и установки сайта на 2-х чистых виртуальных машинах. Плейбук должен содержать в себе roles, group_vars, host_vars, templates, при использовании «чувствительных данных» пользоваться ansible-vault. Сайт может быть один но он должен висеть на разных портах, например: 1-ая машина – nginx запущен на порту 8080, 2-ая машина – nginx запущен на порту 9090 и на этих же портах доступен сайт соответственно.
Так же используя magic variables сделать условие, чтобы плейбук отрабатывал как на CentOS, так и на Ubuntu.

Выполнено:
Развернут nginx + сайт на виртуальной машине ubuntu. На CentOS не проверял, но в теории должно работать.

Как использовать:
 1) Для добавления удаленного хоста необходимо прописать ip в invetory.yml
 2) Запуск плэйбука
    - Подключение по паролю
    ```
    sudo ansible-playbook main.yml -u RemoteUser -i inventory.yml -k -K
    ```
    - С указанием private_key файла
    ```
    sudo ansible-playbook main.yml -u RemoteUser -i inventory.yml -k -K --private-key SshPrivateKeyPath
    ```
где: 
   - RemoteUser - пользователь под которым будут выполняться операции;
   - SshPrivateKeyPath - путь до приватного ssh ключа

Пример работы на виртуальной машин:
![alt text](upNginx.png "VirtualBox Ubuntu up Nginx")
